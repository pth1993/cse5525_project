import torch
import random
import numpy as np
from datareader import DataReader
import torch.optim as optim
import torch.nn as nn


data_path = 'data/'
Train_iter = 10
batch_size = 16
nlabel = 2
hidden_dim = 25
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
random_seed = 1234
use_gpu = torch.cuda.is_available()
embedding = 'glove.6B.50d'

random.seed(random_seed)
torch.manual_seed(random_seed)
np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True


class LSTMClassifier(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, label_size, batch_size, embedding_weights):
        super(LSTMClassifier, self).__init__()
        self.hidden_dim = hidden_dim
        self.batch_size = batch_size
        self.word_embeddings = nn.Embedding.from_pretrained(embedding_weights)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim)
        self.hidden2label = nn.Linear(hidden_dim, label_size)

    def forward(self, sentence):
        embeds = self.word_embeddings(sentence)
        out, (hidden, cell) = self.lstm(embeds)
        hidden = hidden.squeeze(0)
        y = self.hidden2label(hidden)
        return y


def Train_LSTM(data, epochs, batch_size, embedding_weights):
    model = LSTMClassifier(embedding_dim=embedding_weights.shape[1],hidden_dim=hidden_dim,
                           label_size=nlabel, batch_size=batch_size, embedding_weights=embedding_weights)
    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    loss_function = nn.CrossEntropyLoss()
    for epoch in range(epochs):
        total_loss = 0.0
        for i, batch in enumerate(data.train_iter):
            feature, label = batch.sentence, batch.label
            optimizer.zero_grad()
            output = model(feature)
            loss = loss_function(output, label)
            total_loss += loss
            loss.backward()
            optimizer.step()
        print(f"loss on epoch {epoch} = {total_loss}")
    return model


if __name__ == "__main__":
    data = DataReader(data_path=data_path, batch_size=batch_size, device=device, embedding=embedding)
    embedding_vectors = data.embedding
    MLP = Train_LSTM(data, Train_iter, batch_size, embedding_vectors)


