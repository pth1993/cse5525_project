import spacy
from torchtext.data import Field
from torchtext.data import TabularDataset
from torchtext.data import BucketIterator
import csv
import numpy as np


spacy_en = spacy.load('en')


class DataReader(object):
    def __init__(self, data_path, batch_size, device, embedding=None):
        self.TEXT = Field(sequential=True, tokenize=self.tokenize, lower=True, include_lengths = True)
        self.LABEL = Field(sequential=False, use_vocab=False)
        self.datafield = [("id", None), ("sentence", self.TEXT), ("label", self.LABEL)]
        train_data, dev_data_a, test_data_a = TabularDataset.splits(path=data_path, train='train.csv',
                                                                    validation="dev_a.csv", test="test_a.csv",
                                                                    format='csv', skip_header=True, fields=self.datafield)
        dev_data_b, test_data_b = TabularDataset.splits(path=data_path, validation="dev_b.csv", test="test_b.csv",
                                                        format='csv', skip_header=True, fields=self.datafield)
        self.TEXT.build_vocab(train_data)
        if embedding:
            self.TEXT.vocab.load_vectors(embedding)
            self.embedding = self.TEXT.vocab.vectors.to(device)
        else:
            self.embedding = None
        # self.train_iter, self.val_iter = BucketIterator(train_data, batch_size=batch_size, device=device,
        #                                                 sort_key=lambda x: len(x.sentence), sort_within_batch=True)
        self.train_iter, self.dev_a_iter, self.test_a_iter, self.dev_b_iter, self.test_b_iter = \
            BucketIterator.splits((train_data, dev_data_a, test_data_a, dev_data_b, test_data_b), batch_size=batch_size,
                                  sort_within_batch=True, sort_key=lambda x: len(x.sentence), device=device)

    def tokenize(self, text):
        return [tok.text for tok in spacy_en.tokenizer(text)]


class DataReaderELMo(object):
    def __init__(self, data_path, batch_size):
        train_data = self.read_data(data_path + 'train.csv')
        dev_data_a = self.read_data(data_path + 'dev_a.csv')
        test_data_a = self.read_data(data_path + 'test_a.csv')
        dev_data_b = self.read_data(data_path + 'dev_b.csv')
        test_data_b = self.read_data(data_path + 'test_b.csv')
        self.train_iter = self.get_batch_data(train_data, batch_size)
        self.dev_a_iter = self.get_batch_data(dev_data_a, batch_size)
        self.test_a_iter = self.get_batch_data(test_data_a, batch_size)
        self.dev_b_iter = self.get_batch_data(dev_data_b, batch_size)
        self.test_b_iter = self.get_batch_data(test_data_b, batch_size)
    
    def tokenize(self, text):
        return [tok.text for tok in spacy_en.tokenizer(text)]

    def read_data(self, input_file):
        id = []
        sentence = []
        label = []
        with open(input_file) as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            headers = next(csvreader, None)
            for row in csvreader:
                id.append(row[0])
                sentence.append(self.tokenize(row[1]))
                label.append(int(row[2]))
        return {'sentence': np.array(sentence), 'label': np.array(label)}

    def get_batch_data(self, data, batch_size):
        num_data = len(data['sentence'])
        index = np.random.permutation(num_data)
        for start_idx in range(0, num_data, batch_size):
            excerpt = index[start_idx:start_idx + batch_size]
            yield data['sentence'][excerpt].tolist(), data['label'][excerpt]


if __name__ == '__main__':
    datareader = DataReaderELMo(data_path='data/', batch_size=16)
    for i, batch in enumerate(datareader.train_iter):
        sentence, label = batch
        print(sentence)
        if i == 0:
            break