import torch
import random
import numpy as np
from datareader import DataReader

data_path = 'data/'
batch_size = 16
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
random_seed = 1234
# embedding is chosen from ['glove.6B.50d', 'glove.6B.100d', 'glove.6B.200d', 'glove.6B.300d'] or None
# access embedding from DataReader.embedding
embedding = 'glove.6B.50d'

random.seed(random_seed)
torch.manual_seed(random_seed)
np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True

data = DataReader(data_path=data_path, batch_size=batch_size, device=device, embedding=embedding)
embedding_vectors = data.embedding
for i, batch in enumerate(data.dev_b_iter):
    (feature, batch_length), label = batch.sentence, batch.label
    print(label)
